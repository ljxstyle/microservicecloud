package com.ljx.springcloud.dao;

import com.ljx.springcloud.entities.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DeptDao {

    boolean addDept(Dept dept);

    Dept findById(Long deptno);

    List<Dept> findAll();
}
