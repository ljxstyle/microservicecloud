package com.ljx.springcloud.service;

import com.ljx.springcloud.entities.Dept;

import java.util.List;


public interface DeptService {
    boolean addDept(Dept dept);

    Dept findById(Long deptno);

    List<Dept> findAll();
}
