package com.ljx.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZuulGateWay9527App {
    public static void main(String[] arg){
        SpringApplication.run(ZuulGateWay9527App.class,arg);
    }
}
