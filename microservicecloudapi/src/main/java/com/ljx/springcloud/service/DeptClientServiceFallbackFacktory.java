package com.ljx.springcloud.service;

import com.ljx.springcloud.entities.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DeptClientServiceFallbackFacktory implements FallbackFactory<DeptClientService> {
    public DeptClientService create(Throwable throwable) {
        return new DeptClientService() {
            public List<Dept> findAll() {
                List<Dept> depts = new ArrayList<Dept>();
                depts.add(new Dept().setDname("name is null in facktory").setDeptno(0L).setDb_source("no db"));
                return depts;
            }

            public boolean addDept(Dept dept) {
                return false;
            }

            public Dept findById(Long deptno) {
                return new Dept().setDname("name is null in facktory").setDeptno(deptno).setDb_source("no db");
            }
        };
    }
}
