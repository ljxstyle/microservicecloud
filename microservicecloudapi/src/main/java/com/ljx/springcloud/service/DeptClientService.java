package com.ljx.springcloud.service;

import com.ljx.springcloud.entities.Dept;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "microservicecloud-provider-dept",fallbackFactory = DeptClientServiceFallbackFacktory.class)
public interface DeptClientService {

    @RequestMapping(value = "/dept/all",method = RequestMethod.GET)
    List<Dept> findAll();

    @RequestMapping(value = "/dept/add",method = RequestMethod.POST)
    boolean addDept(Dept dept);

    @RequestMapping(value = "/dept/get/{id}",method = RequestMethod.GET)
    Dept findById(@PathVariable("id") Long id);

}
