package com.ljx.springcloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.ljx.springcloud"})
@ComponentScan("com.ljx.springcloud")
public class DepConsumerfeignApp {
    public static void main(String[] arg){
        SpringApplication.run(DepConsumerfeignApp.class,arg);
    }
}
