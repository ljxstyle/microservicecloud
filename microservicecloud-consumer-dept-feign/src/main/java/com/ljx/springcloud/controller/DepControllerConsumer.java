package com.ljx.springcloud.controller;


import com.ljx.springcloud.entities.Dept;
import com.ljx.springcloud.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class DepControllerConsumer {

    @Autowired
    private DeptClientService deptClientService;

    @RequestMapping(value = "/consumer/all",method = RequestMethod.GET)
    @ResponseBody
    public List<Dept> getAll(){
        return deptClientService.findAll();
    }

    @RequestMapping(value = "/consumer/get/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Dept get(@PathVariable("id") Long id){
        return deptClientService.findById(id);
    }

}
