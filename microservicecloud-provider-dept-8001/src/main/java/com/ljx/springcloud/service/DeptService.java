package com.ljx.springcloud.service;

import com.ljx.springcloud.entities.Dept;
import org.springframework.stereotype.Service;

import java.util.List;


public interface DeptService {
    boolean addDept(Dept dept);

    Dept findById(Long deptno);

    List<Dept> findAll();
}
