package com.ljx.springcloud.service.impl;

import com.ljx.springcloud.dao.DeptDao;
import com.ljx.springcloud.entities.Dept;
import com.ljx.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    public boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    public Dept findById(Long deptno) {
        return deptDao.findById(deptno);
    }

    public List<Dept> findAll() {
        return deptDao.findAll();
    }
}
