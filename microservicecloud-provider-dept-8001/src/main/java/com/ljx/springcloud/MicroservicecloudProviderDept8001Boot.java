package com.ljx.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class MicroservicecloudProviderDept8001Boot {
    public static void main(String[] arg){
        SpringApplication.run(MicroservicecloudProviderDept8001Boot.class,arg);
    }
}
