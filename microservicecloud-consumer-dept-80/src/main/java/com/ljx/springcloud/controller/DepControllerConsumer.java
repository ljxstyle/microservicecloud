package com.ljx.springcloud.controller;

import com.ljx.springcloud.entities.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class DepControllerConsumer {

    //private static final String PROVIDERURL = "http://localhost:8001";

    private static final String PROVIDERURL = "http://microservicecloud-provider-dept";

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/all")
    public List<Dept> getAll(){
        return restTemplate.getForObject(PROVIDERURL+"/dept/all",List.class);
    }
}
