package com.ljx.springcloud;

import com.ljx.myrule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "microservicecloud-provider-dept",configuration = MySelfRule.class)
public class DepConsumerApp {
    public static void main(String[] arg){
        SpringApplication.run(DepConsumerApp.class,arg);
    }
}
